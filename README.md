# koala
Utilities for reading, writing, and converting common data file formats. *This
project is still very young! Expect new releases/features on a very regular
basis.*

# Documentation
 - [Snippets](https://gitlab.com/ryukoposting/koala/snippets)
 - [Docs](http://ryukoposting.gitlab.io/koala-docs)

# Goals
**General Goals:**
  - Provide a compact class framework which can be used to describe the contents
    of many data file formats.
  - Parse data files of many different types into this framework.
  - Use this framework to write out data files in a user-specified format.
  - Facilitate the "transformation" of a data file by loading its contents,
    then writing out the generated data structure into a file of a different
    format.

**API Goals:**
  - Facilitate the use of Kotlin's functional programming idioms by providing
    an assortment of higher-order function properties.
  - Avoid use of nullable object types to improve ease of use for the user.
  - Use "value-only" mutability for individual data points, wherein all
    individual data points are mutable, but each data point's respective type
    cannot be changed.

**File format support:**
  - Native support for CSV, INI, YAML, and TOML formats.
  - Native support for "CSV" files which use non-comma delimiters.
  - JSON support through interoperability with Cedric Beust's
    [Klaxon](https://github.com/cbeust/klaxon) JSON library.
  - Long-term: Native support for JSON and CSON.

# Using koala
**build.gradle:**
```gradle
repositories {
    maven { url "https://dl.bintray.com/ryukoposting/koala/" }
}

dependencies {
    compile 'io.ryukoposting:koala:VERSION'
}
```
Latest version: 0.0.1

**pom.xml**
```xml
<repositories>
    <repository>
        <id>koala.ryukoposting.io</id>
        <url>https://dl.bintray.com/ryukoposting/koala/</url>
    </repository>
</repositories>

<dependency>
    <groupId>io.ryukoposting</groupId>
    <artifactId>koala</artifactId>
    <version>VERSION</version>
</dependency>
```