import io.ryukoposting.koala.file.csv.BlockCSV
import io.ryukoposting.koala.file.csv.ColumnCSV
import io.ryukoposting.koala.structure.child.DoubleChild
import io.ryukoposting.koala.structure.child.IntegerChild
import io.ryukoposting.koala.structure.child.ListChild
import io.ryukoposting.koala.structure.child.StringChild
import org.junit.*

class CSVTest {

    // Verify that ColumnCSV parses strings into the correct data types, and follows expected structure.
    @Test
    fun columnCSV() {
        val headers = listOf("id", "first_name", "last_name", "email", "gender", "ip_address")
        ColumnCSV().apply {
            readFromString("id,first_name,last_name,email,gender,ip_address\n" +
                    "19,Daniella,Taphouse,dtaphouse0@eventbrite.com,Female,29.40.168.213\n" +
                    "45,Viva,Loutheane,vloutheane1@geocities.com,Female,98.61.251.87\n" +
                    "63,Ofella,Sollowaye,osollowaye2@icq.com,Female,128.34.63.209\n" +
                    "92,Gibby,Crittal,gcrittal3@free.fr,Male,241.32.137.115\n" +
                    "74,Dieter,Basford,dbasford4@netscape.com,Male,239.209.218.107\n" +
                    "58,Marge,Fulbrook,mfulbrook5@nydailynews.com,Female,67.207.67.48\n" +
                    "89,Margeaux,Snowdon,msnowdon6@si.edu,Female,104.204.77.110\n" +
                    "29,Renaud,McDavitt,rmcdavitt7@goo.ne.jp,Male,236.6.138.113\n" +
                    "93,Marybelle,Hazeup,mhazeup8@reference.com,Female,247.148.161.88\n" +
                    "13,Jasen,Beau,jbeau9@eventbrite.com,Male,181.172.76.34\n" +
                    "6,Beulah,O'Hederscoll,bohederscolla@foxnews.com,Female,198.201.209.9\n" +
                    "91,Frederigo,Noyes,fnoyesb@acquirethisname.com,Male,42.90.74.199\n" +
                    "20,Manda,Scalera,mscalerac@yolasite.com,Female,20.117.119.55\n" +
                    "67,Mano,Struys,mstruysd@upenn.edu,Male,170.31.41.16\n" +
                    "86,Jenda,Wear,jweare@earthlink.net,Female,132.48.99.27\n" +
                    "16,Xylina,Ricold,xricoldf@wordpress.org,Female,32.150.10.44\n" +
                    "83,Louis,Hanks,lhanksg@sfgate.com,Male,182.239.236.168\n" +
                    "67,Sayre,Pughe,spugheh@fotki.com,Male,152.15.89.183\n" +
                    "7,Haze,Corradetti,hcorradettii@amazonaws.com,Male,89.95.130.182\n" +
                    "55,Leif,Dederich,ldederichj@marriott.com,Male,99.19.171.76\n")
            assert(parents.size == 6, message("parents does not have the expected number of headers. parents = $parents"))
            parents.forEachIndexed { colIndex, it ->
                assert(it.child is ListChild, message("Parent.Child is not of type ListChild! column: $colIndex child: $it"))
                assert(it.name == headers[colIndex], message("Parent name is incorrect! expected: ${headers[colIndex]} found: ${it.name}"))
                assert(hasParentName(headers[colIndex]), message("hasParentName() returned incorrect value for column:$colIndex expected:${headers[colIndex]} found: ${it.name}"))
                assert(indexOf(headers[colIndex]) == colIndex, message("indexOf() returned incorrect value for column:$colIndex expected:${headers[colIndex]} found: ${it.name}\""))
                when(colIndex) {
                    0 -> it.childList().forEachIndexed{ rowIndex, iit ->
                        assert(iit is IntegerChild, message("Parent.Child is not IntegerChild! column: $colIndex row: $rowIndex child: $iit"))

                        assert(iit.value is Int, message("Parent.Child.value is not Int! column: $colIndex row: $rowIndex child: $iit"))

                        assert(iit == child(colIndex, rowIndex), message("child($colIndex,$rowIndex) returned unexpected value! expected: ${iit.value} found: ${get(colIndex, rowIndex)}"))
                        assert(iit.value == get(colIndex, rowIndex), message("get($colIndex,$rowIndex) returned unexpected value! expected: ${iit.value} found: ${get(colIndex, rowIndex)}"))
                        assert(iit.value == int(colIndex, rowIndex), message("int($colIndex,$rowIndex) returned unexpected value! expected: ${iit.value} found: ${int(colIndex, rowIndex)}"))
                        assert(iit.value == number(colIndex, rowIndex), message("number($colIndex,$rowIndex) returned unexpected value! expected: ${iit.value} found: ${number(colIndex, rowIndex)}"))

                        assert(iit == child(headers[colIndex], rowIndex), message("child(${headers[colIndex]},$rowIndex) returned unexpected child! expected: $iit found: ${child(headers[colIndex], rowIndex)}"))
                        assert(iit.value == get(headers[colIndex], rowIndex), message("get(${headers[colIndex]},$rowIndex) returned unexpected value! expected: ${iit.value} found: ${get(headers[colIndex], rowIndex)}"))
                        assert(iit.value == int(headers[colIndex], rowIndex), message("int(${headers[colIndex]},$rowIndex) returned unexpected value! expected: ${iit.value} found: ${int(headers[colIndex], rowIndex)}"))
                        assert(iit.value == number(headers[colIndex], rowIndex), message("number(${headers[colIndex]},$rowIndex) returned unexpected value! expected: ${iit.value} found: ${number(headers[colIndex], rowIndex)}"))
                    }
                    else -> it.childList().forEachIndexed{ rowIndex, iit ->
                        assert(iit is StringChild, message("Parent.Child is not StringChild! column: $colIndex row: $rowIndex child: $iit"))

                        assert(iit.value is String, message("Parent.Child.value is not String! column: $colIndex row: $rowIndex child: $iit"))

                        assert(iit == child(colIndex, rowIndex), message("child($colIndex,$rowIndex) returned unexpected child! expected: $iit found: ${child(colIndex, rowIndex)}"))
                        assert(iit.value == get(colIndex, rowIndex), message("get($colIndex,$rowIndex) returned unexpected value! expected: ${iit.value} found: ${get(colIndex, rowIndex)}"))
                        assert(iit.value == string(colIndex, rowIndex), message("string($colIndex,$rowIndex) returned unexpected value! expected: ${iit.value} found: ${string(colIndex, rowIndex)}"))

                        assert(iit == child(headers[colIndex], rowIndex), message("child(${headers[colIndex]},$rowIndex) returned unexpected child! expected: $iit found: ${child(headers[colIndex], rowIndex)}"))
                        assert(iit.value == get(headers[colIndex], rowIndex), message("get(${headers[colIndex]},$rowIndex) returned unexpected value! expected: ${iit.value} found: ${get(headers[colIndex], rowIndex)}"))
                        assert(iit.value == string(headers[colIndex], rowIndex), message("string(${headers[colIndex]},$rowIndex) returned unexpected value! expected: ${iit.value} found: ${string(headers[colIndex], rowIndex)}"))
                    }
                }
                assert(it.child == it.listChild(), message("<Parent>.(child: ListChild) does not match it.listChild()!"))
                assert(it.child.value == it.childList(), message("<Parent>.(child: ListChild).value does not match it.childList()"))
                assert(it.childList().size == 20, message("Parent at index $colIndex has incorrect number of elements: ${it.childList().size} (expected: 20)"))
            }
        }
    }

    // Verify that ColumnCSV parses strings into the correct data types, and follows expected structure.
    @Test
    fun blockCSV() {
        BlockCSV().apply {
            // use column 0 as the parent column
            readFromString("foo, bar, boo, bar, bat\n" +
                    "thing_c, 50, 20,, 0.3\n" +
                    "thing_a, 20, 40, two, \"hello world!\"\n" +
                    "thing_a, 10, 60, three, \"test,me!\"\n" +
                    "thing_b, 30, 80, four,")
            assert(getAll("thing_a", "bar").size == 4)
            assert(getAll("thing_a", "bar").contains(20))
            assert(getAll("thing_a", "bar").contains("two"))
            assert(getAll("thing_a", "bar").contains(10))
            assert(getAll("thing_a", "bar").contains("three"))
            assert(pivot("thing_c")?.size == 3)
            assert(pivot("thing_a")?.size == 4)
            assert(pivots("thing_a").size == 2)
            assert(pivot("thing_b")?.size == 3)
            assert(children("thing_c", "bar").size == 1)

            assert(parents.size == 4)

            assert(get("thing_b", "boo") is Int)
            assert(get("thing_b", "boo") == 80)
            assert(child("thing_b", "boo") is IntegerChild)

            assert(get("thing_c", "bat") is Double)
            assert(get("thing_c", "bat") == 0.3)
            assert(child("thing_c", "bat") is DoubleChild)

            assert(get("thing_b", "bat") == null)
            assert(child("thing_b", "bat") == null)

            children("thing_a", "bat").forEach { assert(it is StringChild) }

            getAll("thing_a", "bat").forEach { assert(it is String) }

            sortByPivot()
            assert(parents[0].name == "thing_a")
            assert(parents[1].name == "thing_a")
            assert(parents[2].name == "thing_b")
            assert(parents[3].name == "thing_c")

            sortByPivotDescending()
            assert(parents[0].name == "thing_c")
            assert(parents[1].name == "thing_b")
            assert(parents[2].name == "thing_a")
            assert(parents[3].name == "thing_a")
        }
    }

}