package io.ryukoposting.koala

import io.ryukoposting.koala.structure.Parent

/**
 * Classes which implement [Structure] represent an arbitrary data structure which is understood by koala.
 *
 * Classes which implement [Structure] will usually also inherit one or both of [Writable] and [Readable], which respectively
 * implement a [Structure]'s output and input functionalities,
 */
interface Structure {
    val parents: MutableList<Parent>
}