package io.ryukoposting.koala

/**
 * Classes which implement [Readable] can import data as a string, or load it from a file.
 */
interface Readable {
    /** Parses [string] as data in a format specified by the subclass' implementation. */
    fun readFromString(string: String): Unit

    /** Parses the file located at [filePath] in a format specified by the subclass' implementation. */
    fun readFromFile(filePath: String): Unit
}