package io.ryukoposting.koala

/**
 * Classes which implement [Writable] can output strings and files based on their contained data.
 */
interface Writable {
    /** Prints the implementing class' data as a formatted string. */
    fun writeToString(): String

    /** Writes the implementing class' data to a file, overwriting the contents of the file. */
    fun writeToFile(filePath: String): Unit
}