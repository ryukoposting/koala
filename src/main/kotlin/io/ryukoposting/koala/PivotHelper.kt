package io.ryukoposting.koala

import io.ryukoposting.koala.structure.Child
import io.ryukoposting.koala.structure.Parent
import io.ryukoposting.koala.structure.child.*

operator fun MutableList<ParentChild>.set(parentName: String, value: Any) = set(parentName, 0, value)

inline fun MutableList<ParentChild>.forEach(action: (name: String, value: Any) -> Unit) = forEach{
    val (p, c) = it.value
    action(p, c.value)
}

inline fun MutableList<ParentChild>.forEachIndexed(action: (index: Int, name: String, value: Any) -> Unit) = forEachIndexed {
    i, it ->
    val (p, c) = it.value
    action(i, p, c.value)
}

operator fun MutableList<ParentChild>.get(parentName: String) = find{ it.parent().name == parentName }?.parent()?.child!!.value

operator fun MutableList<ParentChild>.get(parentName: String, index: Int) = getAll(parentName)[index].parent().child.value

fun MutableList<ParentChild>.getAll(parentName: String) = filter{ it.parent().name == parentName }

operator fun MutableList<ParentChild>.set(parentName: String, index: Int, value: Any) {
    filter{ it.parent().name == parentName }.apply {
        if(size == 0) throw IllegalArgumentException("No pair with name $parentName")
        else get(index).value.child.let {
            when(it) {
                is DoubleChild -> it.value = value as Double
                is IntegerChild -> it.value = value as Int
                is ListChild -> it.value = value as MutableList<Child>
                is LongChild -> it.value = value as Long
                is ParentChild -> it.value = value as Parent
                is StringChild -> it.value = value as String
                else -> throw IllegalStateException("Child type is not specified for mutation!")
            }
        }
    }
}

fun MutableList<ParentChild>.add(parentName: String, value: Any) {
    add(ParentChild(Parent(parentName, when(value) {
        is Double -> DoubleChild(value)
        is Int -> IntegerChild(value)
        is MutableList<*> -> ListChild(value as MutableList<Child>)
        is Long -> LongChild(value)
        is Parent -> ParentChild(value)
        is String -> StringChild(value)
        else -> throw IllegalArgumentException("Invalid type supplied for value parameter.")
    })))
}

fun MutableList<ParentChild>.remove(parentName: String): Any = remove(find{ it.parent().name == parentName })