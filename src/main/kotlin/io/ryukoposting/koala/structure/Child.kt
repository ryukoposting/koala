package io.ryukoposting.koala.structure

import io.ryukoposting.koala.structure.child.*

/**
 * The interface defining the structure for a child in a parent/child pair.
 *
 * # Test
 * The parent/child pair concept naturally begs a couple of questions regarding its practicality. It differs only slightly
 * from a key/value pair, but the slight differences are what lend the Koala package its versatility.
 *
 * In a Kotlin Map object, all keys are the same type, and all values are the same type. However, in many data file formats,
 * a list of keys could have values of many different types. Many statically-typed languages employ a root class to solve
 * this problem (Kotlin and Scala have Any, Java has Object, etc). For most purposes, this is an excellent solution. However,
 * because koala must be able to parse and interpret data supplied to it as a string, it must be able to match that string to
 * one of some set of data types based on the specific rules of the given format, while still providing null-safety. This
 * requires dozens of smartcasts and exception-catches.
 *
 * However, with the Child interface, koala attempts to alleviate some of the need for these superfluous and time-consuming
 * activities by providing a strict set of allowed data types, as well as a set of customizable rules for the treatment of
 * these respective types. Because each Parent's child is immutable, the specified types of any given parent/child pair remain
 * constrained to types that the parser can correctly interpret.
 */
interface Child {
    companion object {
        fun parseString(string: String,
                        stringChildRule:  (string: String) -> Boolean = { it -> it.startsWith("\"") && it.endsWith("\"") },
                        integerChildRule: (string: String) -> Boolean = { it -> it.toIntOrNull() != null },
                        longChildRule:    (string: String) -> Boolean = { it -> it.toLongOrNull() != null },
                        doubleChildRule:  (string: String) -> Boolean = { it -> it.toDoubleOrNull() != null },
                        listChildRule:    (string: String) -> Boolean = { it -> it.startsWith("[") && it.endsWith("]") },
                        parentChildRule:  (string: String) -> Boolean = { it -> false } ): Child = when {
            stringChildRule(string) -> StringChild(value = string.removeSurrounding("\""))
            integerChildRule(string) -> IntegerChild(value = string.toInt())
            longChildRule(string) -> LongChild(value = string.toLong())
            doubleChildRule(string) -> DoubleChild(value = string.toDouble())
            listChildRule(string) -> ListChild(value = parseStringIntoListChild(string, stringChildRule, integerChildRule, longChildRule, doubleChildRule, listChildRule, parentChildRule))
            //parentChildRule(string) -> ParentChild(value = )
            else -> StringChild(value = string)
        }

        private fun parseStringIntoListChild(string: String,
                                             stringChildRule:  (string: String) -> Boolean,
                                             integerChildRule: (string: String) -> Boolean,
                                             longChildRule:    (string: String) -> Boolean,
                                             doubleChildRule:  (string: String) -> Boolean,
                                             listChildRule:    (string: String) -> Boolean,
                                             parentChildRule:  (string: String) -> Boolean): MutableList<Child> {
            val out = mutableListOf<Child>()
            string.removeSuffix("]").removePrefix("[")
                    .split(regex = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)".toRegex()).forEach {
                out.add(parseString(it, stringChildRule, integerChildRule, longChildRule, doubleChildRule, listChildRule, parentChildRule))
            }
            return out
        }

        // makeString rules cannot be stored with the child because they vary not only based on the stored value's type,
        // but on the specified file format.
        fun makeString(child: Child,
                       stringChildFormat:  (child: StringChild)  -> String = { it -> "\"${it.value}\"" },
                       integerChildFormat: (child: IntegerChild) -> String = { it -> "${it.value}" },
                       longChildFormat:    (child: LongChild)    -> String = { it -> "${it.value}" },
                       doubleChildFormat:  (child: DoubleChild)  -> String = { it -> "${it.value}" },
                       listChildFormat:    (child: ListChild)    -> String = { it ->
                           "[${(it.value.fold("", { str, itt ->
                               val app = makeString(itt, stringChildFormat, integerChildFormat, longChildFormat, doubleChildFormat, parentChildFormat = parentChildFormat)
                               (if(str.isNotEmpty()) "$str,$app" else app) } ))}]" },
                       parentChildFormat:  (child: ParentChild)  -> String = { it -> makeString(it.value.child) }

                        ): String = when(child) {
            is StringChild -> stringChildFormat(child)
            is IntegerChild -> integerChildFormat(child)
            is LongChild -> longChildFormat(child)
            is DoubleChild -> doubleChildFormat(child)
            is ListChild -> listChildFormat(child)
            is ParentChild -> parentChildFormat(child)
            else -> "${child.value}"
        }
    }

    val value: Any

    /**
     * @brief Get the [Double] value stored in the child.
     * @throws ClassCastException if [value] cannot be cast to [Double].
     */
    fun double() = value as Double

    /**
     * @brief Get the [Int] value stored in the child.
     * @throws ClassCastException if [value] cannot be cast to [Int].
     */
    fun int() = value as Int

    /**
     * @brief Get the [MutableList]<[Child]> value stored in the child.
     * @throws ClassCastException if [value] cannot be cast to [MutableList]<[Child]>.
     */
    fun childList() = value as MutableList<Child>

    /**
     * @brief Get the [Long] value stored in the child.
     * @throws ClassCastException if [value] cannot be cast to [Long].
     */
    fun long() = value as Long

    /**
     * @brief Get the [Parent] value stored in the child.
     * @throws ClassCastException if [value] cannot be cast to [Parent].
     */
    fun parent() = value as Parent

    /**
     * @brief Get the [String] value stored in the child.
     * @throws ClassCastException if [value] cannot be cast to [String].
     */
    fun string() = value as String

    /**
     * @brief Get the [Number] value stored in the child.
     * @throws ClassCastException if [value] cannot be cast to [Number].
     */
    fun number() = value as Number

    /**
     * @brief Get this [Child] casted to [DoubleChild]
     * @throws ClassCastException if this Child is not a [DoubleChild]
     */
    fun doubleChild() = this as DoubleChild

    /**
     * @brief Get this [Child] casted to [IntegerChild]
     * @throws ClassCastException if this Child is not a [IntegerChild]
     */
    fun integerChild() = this as IntegerChild

    /**
     * @brief Get this [Child] casted to [ListChild]
     * @throws ClassCastException if this Child is not a [ListChild]
     */
    fun listChild() = this as ListChild

    /**
     * @brief Get this [Child] casted to [LongChild]
     * @throws ClassCastException if this Child is not a [LongChild]
     */
    fun longChild() = this as LongChild

    /**
     * @brief Get this [Child] casted to [ParentChild]
     * @throws ClassCastException if this Child is not a [ParentChild]
     */
    fun parentChild() = this as ParentChild

    /**
     * @brief Get this [Child] casted to [StringChild]
     * @throws ClassCastException if this Child is not a [StringChild]
     */
    fun stringChild() = this as StringChild
}
