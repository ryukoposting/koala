package io.ryukoposting.koala.structure

import io.ryukoposting.koala.structure.child.*

/**
 * @property name The key of the "key-value" pair represented by a parent and child
 * @property child The value of the key-value pair represented by a parent and child
 * Speaking in JSON terms, think of Parent as a key-value pair, where [name] is the key
 * and [child] is the value.
 */
data class Parent(val name: String, val child: Child) {
    /**
     * @brief Get the [Double] value stored in [child].
     * @throws ClassCastException if [value] cannot be cast to [Double].
     */
    fun double() = child.double()

    /**
     * @brief Get the [Int] value stored in [child].
     * @throws ClassCastException if [value] cannot be cast to [Int].
     */
    fun int() = child.int()

    /**
     * @brief Get the [MutableList]<[Child]> value stored in [child].
     * @throws ClassCastException if [value] cannot be cast to [MutableList]<[Child]>.
     */
    fun childList() = child.childList()

    /**
     * @brief Get the [Long] value stored in [child].
     * @throws ClassCastException if [value] cannot be cast to [Long].
     */
    fun long() = child.long()

    /**
     * @brief Get the [Parent] value stored in [child].
     * @throws ClassCastException if [value] cannot be cast to [Parent].
     */
    fun parent() = child.parent()

    /**
     * @brief Get the [String] value stored in [child].
     * @throws ClassCastException if [value] cannot be cast to [String].
     */
    fun string() = child.string()

    /**
     * @brief Get the [Number] value stored in [child].
     * @throws ClassCastException if [value] cannot be cast to [Number].
     */
    fun number() = child.number()

    /**
     * @brief Get [child] casted to [DoubleChild]
     * @throws ClassCastException if this [child] is not a [DoubleChild]
     */
    fun doubleChild() = child as DoubleChild

    /**
     * @brief Get [child] casted to [IntegerChild]
     * @throws ClassCastException if this [child] is not a [IntegerChild]
     */
    fun integerChild() = child as IntegerChild

    /**
     * @brief Get [child] casted to [ListChild]
     * @throws ClassCastException if this [child] is not a [ListChild]
     */
    fun listChild() = child as ListChild

    /**
     * @brief Get [child] casted to [LongChild]
     * @throws ClassCastException if this [child] is not a [LongChild]
     */
    fun longChild() = child as LongChild

    /**
     * @brief Get [child] casted to [ParentChild]
     * @throws ClassCastException if this [child] is not a [ParentChild]
     */
    fun parentChild() = child as ParentChild

    /**
     * @brief Get [child] casted to [StringChild]
     * @throws ClassCastException if this [child] is not a [StringChild]
     */
    fun stringChild() = child as StringChild
}