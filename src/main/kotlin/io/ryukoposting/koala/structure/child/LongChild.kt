package io.ryukoposting.koala.structure.child

import io.ryukoposting.koala.structure.Child

data class LongChild(override var value: Long): Child {
    override fun toString() = value.toString()
}