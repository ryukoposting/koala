package io.ryukoposting.koala.structure.child

import io.ryukoposting.koala.structure.Child

data class DoubleChild(override var value: Double): Child {
    override fun toString() = value.toString()
}