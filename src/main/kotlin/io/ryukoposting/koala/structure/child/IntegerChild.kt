package io.ryukoposting.koala.structure.child

import io.ryukoposting.koala.structure.Child

data class IntegerChild(override var value: Int): Child {
    override fun toString() = value.toString()
}