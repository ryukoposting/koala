package io.ryukoposting.koala.structure.child

import io.ryukoposting.koala.structure.Child

data class StringChild(override var value: String): Child {
    override fun toString() = value
}