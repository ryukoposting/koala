package io.ryukoposting.koala.structure.child

import io.ryukoposting.koala.structure.Child
import io.ryukoposting.koala.structure.Parent

data class ParentChild(override var value: Parent): Child {
    override fun toString() = value.toString()
}