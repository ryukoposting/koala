package io.ryukoposting.koala.structure.child

import io.ryukoposting.koala.structure.Child
import io.ryukoposting.koala.structure.Parent

/**
 * @brief A [Child] with a mutable list for its value. Has overrides for all [kotlin.collections.MutableList] functions.
 */
data class ListChild(override var value: MutableList<Child>): Child {

    /** @brief Iterates only though children in [value] which are type [IntegerChild]. */
    inline fun forEachIntegerChild(action: (elem: IntegerChild) -> Unit) =
            value.forEach{ if (it is IntegerChild) action(it) }

    /** @brief Iterates only though children in [value] which are type [LongChild]. */
    inline fun forEachLongChild(action: (elem: LongChild) -> Unit) =
            value.forEach{ if (it is LongChild) action(it) }

    /** @brief Iterates only though children in [value] which are type [DoubleChild]. */
    inline fun forEachDoubleChild(action: (elem: DoubleChild) -> Unit) =
            value.forEach{ if (it is DoubleChild) action(it) }

    /** @brief Iterates only though children in [value] which are type [ParentChild]. */
    inline fun forEachParentChild(action: (elem: ParentChild) -> Unit) =
            value.forEach{ if (it is ParentChild) action(it) }

    /** @brief Iterates only though children in [value] which are type [StringChild]. */
    inline fun forEachStringChild(action: (elem: StringChild) -> Unit) =
            value.forEach{ if (it is StringChild) action(it) }

    /** @brief Iterates only through children in [value] which are type [IntegerChild], [LongChild], or [DoubleChild]. */
    inline fun forEachNumber(action: (elem: Number) -> Unit) =
            value.forEach{ if (it.value is Number) action(it.value as Number) }

    /** @brief Iterates only though children in [value] which are type [IntegerChild]. Indexes given are relative to the whole, unfiltered list. */
    inline fun forEachIntegerChildIndexed(action: (index: Int, elem: IntegerChild) -> Unit) =
            value.forEachIndexed{ index, child -> if (child is IntegerChild) action(index, child) }

    /** @brief Iterates only though children in [value] which are type [LongChild]. Indexes given are relative to the whole, unfiltered list. */
    inline fun forEachLongChildIndexed(action: (index: Int, elem: LongChild) -> Unit) =
            value.forEachIndexed{ index, child -> if (child is LongChild) action(index, child) }

    /** @brief Iterates only though children in [value] which are type [DoubleChild]. Indexes given are relative to the whole, unfiltered list. */
    inline fun forEachDoubleChildIndexed(action: (index: Int, elem: DoubleChild) -> Unit) =
            value.forEachIndexed{ index, child -> if (child is DoubleChild) action(index, child) }

    /** @brief Iterates only though children in [value] which are type [ParentChild]. Indexes given are relative to the whole, unfiltered list. */
    inline fun forEachParentChildIndexed(action: (index: Int, elem: ParentChild) -> Unit) =
            value.forEachIndexed{ index, child -> if (child is ParentChild) action(index, child) }

    /** @brief Iterates only though children in [value] which are type [StringChild]. Indexes given are relative to the whole, unfiltered list. */
    inline fun forEachStringChildIndexed(action: (index: Int, elem: StringChild) -> Unit) =
            value.forEachIndexed{ index, child -> if (child is StringChild) action(index, child) }

    /** @brief Iterates through the [value]s of children which are type [IntegerChild], [LongChild], or [DoubleChild]. Indexes are relative to the whole, unfiltered list. */
    inline fun forEachNumberIndexed(action: (index: Int, elem: Number) -> Unit) =
            value.forEachIndexed{ index, child -> if (child.value is Number) action(index, child.value as Number) }

    /** @brief Works exactly the same as a normal forEach. */
    inline fun forEach(action: (elem: Child) -> Unit) = value.forEach{ action(it) }

    /** @brief Works exactly the same as a normal forEachIndexed. */
    inline fun forEachIndexed(action: (index: Int, elem: Child) -> Unit) = value.forEachIndexed{ i, o -> action(i, o) }

    override fun toString() = value.toString()
}


