package io.ryukoposting.koala.file.csv

import io.ryukoposting.koala.Readable
import io.ryukoposting.koala.Structure
import io.ryukoposting.koala.Writable
import io.ryukoposting.koala.structure.Child
import io.ryukoposting.koala.structure.Parent
import io.ryukoposting.koala.structure.child.ListChild
import java.io.File

/**
 * ColumnCSV reads CSV files into a structure for reading with standard coordinates.
 *
 * ### Use Cases
 * ColumnCSV should be used in any situation where the ability to access a value in a CSV by its numerical
 * coordinates is needed, or when operations need to be applied to an entire column, or when the specification
 * of a single pivot column (as in [BlockCSV]) is undesirable.
 *
 */
class ColumnCSV: Readable, Writable, Structure {
    /** Container for the complete data structure. */
    override val parents = mutableListOf<Parent>()

    /**
     * Parse [string] as a CSV-formatted string.
     * @see [Readable.readFromString]
     */
    override fun readFromString(string: String) {
        val rows = string.trim().split("\n")
        rows[0].split(regex = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)".toRegex()).forEachIndexed { index, str ->
            parents.add(Parent(str.trim(), ListChild(mutableListOf())))
        }

        for(line in (1 until rows.size))
            rows[line].split(regex = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)".toRegex()).forEachIndexed { index, str ->
                (parents[index].child as? ListChild)?.value?.add(Child.parseString(string = str.trim(), listChildRule = { false }))
            }
    }

    /**
     * Parse a CSV-formatted file.
     * @see [Readable.readFromFile]
     */
    override fun readFromFile(filePath: String) {
        readFromString(File(filePath).readText())
    }

    /**
     * Turns contained data into a CSV-formatted string.
     * @see [Writable.writeToString]
     */
    override fun writeToString(): String {
        val tableHeight = (parents.maxBy{ (it.child as? ListChild)?.value?.size ?: 0 }?.child as? ListChild)?.value?.size
                ?: throw IllegalStateException("writeToString() cannot be called on empty ColumnCSV!")
        var outString: String = ""
        val quoteIfSpecialCharacters: (str: String) -> String = {
            if(it.contains("[,]".toRegex()) && (!it.startsWith("\"")) && (!it.startsWith("\""))) "\"$it\"" else it
        }
        parents.forEach {
            outString += "${(if(outString.isNotEmpty()) "," else "")}${quoteIfSpecialCharacters(it.name)}"
        }
        outString += '\n'
        for(i in 0..(tableHeight-1)) {
            parents.forEach {
                if(outString.last() != '\n') outString += ','
                val p = (it.child as? ListChild)?.value?.getOrNull(i)
                if(p != null) outString += Child.makeString(p, stringChildFormat = {quoteIfSpecialCharacters(p.string())})
            }
            outString += "\n"
        }
        return outString.dropLast(1)
    }

    /**
     * Writes contained data into a CSV-formatted file.
     * @see [Writable.writeToFile]
     */
    override fun writeToFile(filePath: String) {
        File(filePath).printWriter().use { out ->
            out.println(writeToString())
        }
    }

    /**
     * Returns true if [string] is equal to one of the CSV columns' headers.
     * @return true if [string] is equal to one of the CSV columns' headers
     */
    fun hasParentName(name: String) = parents.any{ it.name == name }

    /**
     * Returns the column number of the column named [columnName].
     * @return the column number of the column named [columnName]. -1 if no columns have a header with this name.
     */
    fun indexOf(columnName: String) = parents.indexOfFirst{ it.name == columnName }

    /**
     * Returns the [Child] wrapper object for the value stored at the specified coordinates.
     * @return the [Child] wrapper object for the value stored at the specified coordinates.
     */
    fun child(column: Int, row: Int) = (parents[column].child.childList())[row]

    /**
     * Returns the [Child] wrapper object for the value stored in the specified column title and row number.
     * @param column The name of the column containing the desired return [Child]
     * @param row The row number of the desired return [Child]
     * @return the [Child] wrapper object for the value stored at the specified location, or null.
     */
    fun child(column: String, row: Int) = (parents.find{ it.name == column }?.child?.childList())?.get(row)

    /** Get the value stored at the specified coordinates. */
    operator fun get(column: Int, row: Int) = child(column, row).value

    /** Get the value stored at the specified column name and row number. */
    operator fun get(column: String, row: Int) = child(column, row)?.value

    /**
     * Get the [String] value stored at the specified coordinates.
     * @throws ClassCastException if the value cannot be cast to a [String]
     */
    fun string(column: Int, row: Int) = child(column, row).string()

    /**
     * Get the [String] value stored at the specified column name and row number.
     * @throws ClassCastException if the value cannot be cast to a [String]
     */
    fun string(column: String, row: Int) = child(column, row)?.string()

    /**
     * Get the [Double] value stored at the specified coordinates.
     * @throws ClassCastException if the value cannot be cast to a [Double]
     */
    fun double(column: Int, row: Int) = child(column, row).double()

    /**
     * Get the [Double] value stored at the specified column name and row number.
     * @throws ClassCastException if the value cannot be cast to a [Double]
     */
    fun double(column: String, row: Int) = child(column, row)?.double()

    /**
     * Get the [Int] value stored at the specified coordinates.
     * @throws ClassCastException if the value cannot be cast to a [Int]
     */
    fun int(column: Int, row: Int) = child(column, row).int()

    /**
     * Get the [Int] value stored at the specified column name and row number.
     * @throws ClassCastException if the value cannot be cast to a [Int]
     */
    fun int(column: String, row: Int) = child(column, row)?.int()

    /**
     * Get the [Long] value stored at the specified coordinates.
     * @throws ClassCastException if the value cannot be cast to a [Long]
     */
    fun long(column: Int, row: Int)  = child(column, row).long()

    /**
     * Get the [Long] value stored at the specified column name and row number.
     * @throws ClassCastException if the value cannot be cast to a [Long]
     */
    fun long(column: String, row: Int)  = child(column, row)?.long()

    /**
     * Get the value stored at the specified coordinates, casted to [Number].
     * @throws ClassCastException if the value cannot be cast to a [Number]
     */
    fun number(column: Int, row: Int) = child(column, row).number()

    /**
     * Get the value stored at the specified column name and row number, casted to [Number].
     * @throws ClassCastException if the value cannot be cast to a [Number]
     */
    fun number(column: String, row: Int) = child(column, row)?.number()

    override fun toString() = "$parents"
}