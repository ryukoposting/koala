package io.ryukoposting.koala.file.csv

import io.ryukoposting.koala.Readable
import io.ryukoposting.koala.Structure
import io.ryukoposting.koala.Writable
import io.ryukoposting.koala.structure.Child
import io.ryukoposting.koala.structure.Parent
import io.ryukoposting.koala.structure.child.*
import java.io.File

/**
 * BlockCSV parses a CSV file into a set of sub-objects, addressable by the value stored in a specific column.
 *
 * ### Use Cases
 * BlockCSV is optimal for CSV files where many rows to not have values for all columns, and for
 * CSV files where the user wishes to address each row as an object with a unique name. BlockCSV is not useful
 * for cases where a specific ordering of columns must be preserved. A BlockCSV does **NOT**
 * create an empty [Child] for each empty cell in the CSV file. Instead, empty cells are simply ignored.
 *
 * ### The Pivot
 * The documentation will regularly refer to the "pivot," which refers to the column used by BlockCSV to to
 * determine the user-addressable name of each row of data. Each row of data as addressed by its pivot. All
 * data in a row is addressed by the value stored in that row's pivot column. Pivots are stored as [String]s,
 * regardless of their contents. **If using BlockCSV, be sure to also import `io.ryukoposting.koala.*`, as there
 * are multiple helper functions relating to pivot usage.
 */
class BlockCSV: Readable, Writable, Structure {
    override val parents = mutableListOf<Parent>()

    /**
     * The header of the pivot column.
     * When the BlockCSV is written out using a function inherited form [Writable], this will be the header
     * for the column where the pivots are placed.
     */
    var pivotColumnTitle = "name"

    /**
     * Parse [string] as a CSV-formatted string, using column 0 as the pivot column.
     * @see [Readable.readFromString]
     */
    override fun readFromString(string: String) = readFromString(string, pivotColumn = 0)

    /**
     * Parse [string] as a CSV-formatted string, using a specified pivot column.
     * @param pivotColumn The index of the column to use as a pivot.
     * @param setPivotColumnTitle If true, replace the value stored in [pivotColumnTitle] with the header of the column specified by [pivotColumn]
     * @see [Readable.readFromString]
     */
    fun readFromString(string: String, pivotColumn: Int = 0, setPivotColumnTitle: Boolean = true) {
        // TODO: give BlockCSV its own parsing algorithm instead of refactoring the output of ColumnCSV
        val csv = ColumnCSV().apply{ readFromString(string = string.trim()) }
        if(setPivotColumnTitle) pivotColumnTitle = csv.parents[pivotColumn].name
        csv.parents[pivotColumn].childList().forEachIndexed { rowIndex, it ->
            // add a parent, all its elements will be stuff from row `index`
            parents.add(rowIndex, Parent("${it.value}", ListChild(mutableListOf())))

            // add parent-child pairs, making sure we don't redundantly add the parent's name
            // column into the child of the parent itself
            csv.parents.filterIndexed { colIndex, _ -> colIndex != pivotColumn  }.forEach {
                if(it.childList()[rowIndex].value != "")
                    parents[rowIndex].childList().add(ParentChild(Parent(it.name, it.childList()[rowIndex])))
            }
        }
    }

    /**
     * Parse a CSV-formatted file, using column 0 as the pivot.
     * @see [Readable.readFromFile]
     */
    override fun readFromFile(filePath: String) = readFromFile(filePath, 0)

    /**
     * Parse a CSV-formatted file, using the specified column as the pivot.
     * @see [Readable.readFromFile]
     */
    fun readFromFile(filePath: String, pivotColumn: Int) = readFromString(File(filePath).readText(), pivotColumn)

    /**
     * Turns contained data into a CSV-formatted string, putting pivots into column 0.
     * @see [Writable.writeToString]
     */
    override fun writeToString(): String = writeToString(0)

    /**
     * Turns contained data into a CSV-formatted string, putting pivots into the specified column.
     * @see [Writable.writeToString]
     */
    fun writeToString(pivotColumn: Int): String {
        var outString = ""
        val quoteIfSpecialCharacters: (str: String) -> String = {
            if(it.contains("[,]".toRegex()) && (!it.startsWith("\"")) && (!it.startsWith("\""))) "\"$it\"" else it
        }
        val headCount = mutableMapOf<String,Int>()
        parents.forEach {
            it.childList().fold(mutableMapOf<String,Int>()) { map, thisChild ->
                if(map.containsKey(thisChild.parent().name)) {
                    map[thisChild.parent().name] = map[thisChild.parent().name]!! + 1
                } else map[thisChild.parent().name] = 1
                map
            }.forEach { (k, v) ->
                if(headCount[k] == null) headCount[k] = v else if(headCount[k]!! < v) headCount[k] = v
            }
        }
        val headers = mutableListOf<String>().apply {
            headCount.forEach {(h, count) ->
                repeat(count) { add(h) }
            }
            add(pivotColumn, pivotColumnTitle)

            // add them to the string
            forEach {
                outString += "${(if(outString.isNotEmpty()) "," else "")}${quoteIfSpecialCharacters(it)}"
            }
        }
        outString += "\n"
        parents.forEach { parent -> if (parent.child is ListChild) {
            headers.forEachIndexed { index, header ->
                if(outString.last() != '\n') outString += ','
                // find the ParentChild whose name corresponds to the header

                val item = if(header == pivotColumnTitle) StringChild(parent.name) else {
                    with(parent.childList().filter { (it as? ParentChild)?.value?.name == header }) {
                        when {
                            size == 1 -> get(0).parent().child
                            isEmpty() -> null
                            else -> {
                                var ct = 0
                                headers.subList(0, index).forEach { if (it == get(0).parent().name) ct++ }
                                get(ct).parent().child
                            }
                        }
                    }
                }
                if(item != null)
                    outString += Child.makeString(item, stringChildFormat = { quoteIfSpecialCharacters(item.string()) }, parentChildFormat = { throw IllegalStateException() })
            }
            outString += "\n"
        } }
        return outString.dropLast(1)
    }

    /**
     * Writes contained data into a CSV-formatted file, placing pivots in column 0.
     * @see [Writable.writeToFile]
     */
    override fun writeToFile(filePath: String) {
        File(filePath).printWriter().use { out ->
            out.println(writeToString())
        }
    }

    /**
     * Writes contained data into a CSV-formatted file, placing pivots in the specified column.
     * @see [Writable.writeToFile]
     */
    fun writeToFile(filePath: String, pivotColumn: Int) {
        File(filePath).printWriter().use { out ->
            out.println(writeToString(pivotColumn))
        }
    }

    /**
     * Get the first pivot with name [pivotName]. The contents of a pivot are simply a [MutableList] of [ParentChild]s.
     */
    fun pivot(pivotName: String) = parents.find{ it.name == pivotName }?.childList() as MutableList<ParentChild>?

    /**
     * Get a list of all pivots with name [pivotName]. The contents of a pivot are simply a [MutableList] of [ParentChild]s.
     */
    fun pivots(pivotName: String): MutableList<MutableList<ParentChild>> {
        val out = mutableListOf<MutableList<ParentChild>>()
        parents.filter{ it.name == pivotName }.forEach {
            out.add(it.childList() as MutableList<ParentChild>)
        }
        return out
    }

    /**
     * Return the first [Child] wrapper object for the value stored at the specified pivot and child name.
     * Returns null if the first pivot matching [pivotName] does not have a child with name [childName]. Also returns null if no
     * pivots have name equal to [pivotName].
     */
    fun child(pivotName: String, childName: String) = try { parents.find{ it.name == pivotName }?.child?.childList()
            ?.filter{ (it as? ParentChild)?.value?.name == childName }?.get(0)?.parent()?.child } catch(e: IndexOutOfBoundsException) { null }

    /**
     * Return all [Child] wrapper objects found with the specified pivotName and child name.
     * Contains values from every child which matches [pivotName] and [childName]. If a single pivot has multiple children for a single
     * [childName], all of them are included in the returned list. If multiple pivots named [pivotName] have children named [childName],
     * all are returned.
     */
    fun children(pivotName: String, childName: String) = parents.filter{ it.name == pivotName }
            .fold(mutableListOf<Child>()) { out, thisParent ->
                out.addAll(thisParent.childList().filter { it.parent().name == childName }
                        .fold(mutableListOf()) { outt, pChild -> outt.add(pChild.parent().child); outt } ); out }

    /**
     * Return the first value that matches the specified pivot name and child name.
     * Will only return data from the first pivotName found with the specified [pivotName]. Returns Null if the first pivot matching [pivotName]
     * does not have a child with name [childName]. Also returns null if no pivots have name equal to [pivotName]
     */
    operator fun get(pivotName: String, childName: String) = child(pivotName, childName)?.value

    /**
     * Return the n'th value that matches the specified pivot name and child name.
     */
    operator fun get(pivotName: String, childName: String, n: Int) = children(pivotName, childName)[n].value

    /**
     * Return a list of all values which match the specified pivot name and child name.
     * The list contains all matching values for all matching pivots, even if a single pivot has multiple children with the same name.
     */
    fun getAll(pivotName: String, childName: String): List<Any> = children(pivotName, childName)
            .fold(mutableListOf()) { out, thisChild -> out.add(thisChild.value); out }

    /**
     * Return the n'th value that matches the specified pivot name anc child name, casted to [String].
     */
    fun string(pivotName: String, childName: String, index: Int) = children(pivotName, childName)[index].string()

    /**
     * Return the n'th value that matches the specified pivot name anc child name, casted to [Double].
     */
    fun double(pivotName: String, childName: String, index: Int) = children(pivotName, childName)[index].double()

    /**
     * Return the n'th value that matches the specified pivot name anc child name, casted to [Int].
     */
    fun int(pivotName: String, childName: String, index: Int) = children(pivotName, childName)[index].int()

    /**
     * Return the n'th value that matches the specified pivot name anc child name, casted to [Long].
     */
    fun long(pivotName: String, childName: String, index: Int) = children(pivotName, childName)[index].long()

    /**
     * Return the n'th value that matches the specified pivot name anc child name, casted to [Number].
     */
    fun number(pivotName: String, childName: String, index: Int) = children(pivotName, childName)[index].number()

    /**
     * Sort the CSV based on each pivot. Works similarly to kotlin-stdlib's [sortBy] function.
     * [selector]'s received `it` value is the string value of the pivot.
     * If [selector] is not passed, sorts in alphabetical order by pivot.
     */
    inline fun <R: Comparable<R>>sortByPivot(crossinline selector: (pivot: String) -> R?) =
            parents.sortBy { selector(it.name) }

    fun sortByPivot() = parents.sortBy{ it.name }

    /**
     * Sort the CSV based on each pivot. Works similarly to kotlin-stdlib's [sortByDescending] function.
     * [selector]'s received `it` value is the string value of the pivot.
     * If [selector] is not passed, sorts in alphabetical order by pivot.
     */
    inline fun <R: Comparable<R>>sortByPivotDescending(crossinline selector: (pivot: String) -> R?) =
            parents.sortBy { selector(it.name) }

    fun sortByPivotDescending() = parents.sortByDescending{ it.name }

    /**
     * Sort based on child values. The first [childName] to be supplied will be last in the sorting process,
     * i.e. it is the first-order sort.
     */
    fun sortByChildren(vararg childNames: String) {
        for(name in childNames.reversed()) {
            when {
                parents.all { it.child.value as? String != null } -> parents.sortBy { it.childList().find { it.parent().name == name }?.parent()?.string() }
                parents.all { it.child.value as? Double != null } -> parents.sortBy { it.childList().find { it.parent().name == name }?.parent()?.double() }
                else -> parents.sortBy { "${it.childList().find { it.parent().name == name }?.parent()?.child}" }
            }
        }

    }

    override fun toString() = "$parents"
}